import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/': (context) => FirstScreen(),
      // When navigating to the "/second" route, build the SecondScreen widget.
      '/second': (context) => SecondScreen(),
    },
  ));
}



class FirstScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.black,

      body: Column(children: <Widget>[
        Row(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.fromLTRB(0, 150, 0, 0),
              child: new Image.asset('assests/images/welcome.jpg',height: 250,width: 360,
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Padding(padding: EdgeInsets.fromLTRB(125, 150, 0, 0),),
            RaisedButton(
              child: Text('Open App',textAlign: TextAlign.center,style: TextStyle(fontSize: 20,fontFamily: 'DancingScript',color: Colors.blueGrey),),
              onPressed: (){
                Navigator.pushNamed(context, '/second');

              },
              color: Colors.black,


            ),
          ],
        )
      ]
      ),
    );
  }
}

class SecondScreen extends StatefulWidget {
  @override
  _MySecondScreen createState() => _MySecondScreen();
}
class _MySecondScreen extends State<SecondScreen> {
  int counter = 0;

  void incrementCounter() {
    setState(() {
      counter++;
    });
  }
  void showToast() {
    Fluttertoast.showToast(
        msg: 'You are on Second Screen',
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 5,
        textColor: Colors.white,
        fontSize: 15,
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: Text("Welcome to Mr.Bean's Blog",
            style: TextStyle(fontFamily: 'Satisfy',fontSize: 20,fontWeight: FontWeight.bold),),
          actions: <Widget>[
            Container(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(right: 31),
                  child: Icon(Icons.child_care,color: Colors.white,),
                ),
              ),
            )

          ],
        ),
        body:Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  new Container(
                    child: new Image.asset('assests/images/mr_bean.jpg',
                        height: 200,
                        width: 360,
                        fit: BoxFit.cover),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  new Container(
                    child: Padding(padding: EdgeInsets.all(16.0),
                      child: IconButton(
                        icon:Icon(Icons.favorite_border,
                          color: Colors.blueGrey,
                        ),
                        onPressed: incrementCounter,
                      ),

                    ),
                  ),
                  SizedBox(
                    width: 18,
                    child: Container(
                      child: Text('$counter',
                        style: TextStyle(color: Colors.white),),

                    ),
                  ),
                  RaisedButton(
                    color: Colors.black,
                    onPressed: showToast,
                    child: Icon(Icons.home,
                      color: Colors.blueGrey,
                      size: 30,
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text('"Action speaks louder than words"-Mr.Bean.'
                        'Rowan Sebastian Atkinson portrayed the role of Mr. Bean the well known person of every children.'
                        'Rowan Atkinson may have more than 50 acting credits on his resume,'
                        'but to most of the world he’ll always be best known as the ridiculously rubber-faced Mr. Bean and is undoubtedly the king of comedy.'
                        'How can we forget that teddy the favourite pet of Mr.bean and apparently best friend.'
                        'He rarely speaks, and when he does, it is generally only a few mumbled words which are in a comically low-pitched voice.'
                        'He also does not like people taking his things.',textAlign: TextAlign.center,
                      style: TextStyle(fontFamily: 'DancingScript',color: Colors.white,fontWeight: FontWeight.bold,fontSize: 18),
                    ),
                  )
                ],

              ),

              Row(
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.black,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      padding: EdgeInsets.fromLTRB(155,30,155,20),
                      child: Icon(Icons.reply,
                        color: Colors.blueGrey,
                        size: 40,
                      ),
                    ),
                  ]
              ),

            ]
        )
    );
  }
}
